## Introduction

This repository contains the code that describes most analyses presented in:

> Fine PVA, Zapata F , and Daly DC. (2014) Investigating processes of Neotropical rain forest 
> tree diversification by examining the evolution and historical biogeography of the Protieae 
> (Burseraceae). Evolution 68: 1988-2004. [doi:10.1111/evo.12414](http://dx.doi.org/10.1111/evo.12414).

## Dependencies

These scripts require R and several libraries. See code in `scripts` for details.

## Running the analyses

The analyses are broken into a series of scripts. Some scripts run analyses, others regenerate figures.
See `scripts` for details.

## Is this a fully executable paper?

This manuscript is partially executable. The code explicitly describes how several analysis 
steps were completed but is not entirely sufficient on its own to re-execute the whole 
paper. There are several reasons for this:

- Figure 1 was prepared manually.

- The code provided here includes paths to already processed files, including phylogenetic trees 
  and data files.

## Data

The `data/` directory contains all the none phylogenetic data and pre-processed files for analyses.
The  `phylogenetic_data` directory contains all the tree and samples of trees necessary to run the 
analyses.